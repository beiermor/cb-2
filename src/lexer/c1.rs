use logos::Logos;

#[derive(Logos, Debug, PartialEq)]
pub enum C1Token {
    // Schlüsselwörter
    #[token("bool")]
    KwBoolean,

    #[token("do")]
    KwDo,

    #[token("else")]
    KwElse,

    #[token("float")]
    KwFloat,

    #[token("for")]
    KwFor,

    #[token("if")]
    KwIf,

    #[token("int")]
    KwInt,

    #[token("printf")]
    KwPrintf,

    #[token("return")]
    KwReturn,

    #[token("void")]
    KwVoid,

    #[token("while")]
    KwWhile,


    // Operatoren
    #[token("+")]
    Plus,

    #[token("-")]
    Minus,

    #[token("*")]
    Asterisk,

    #[token("/")]
    Slash,

    #[token("=")]
    Assign,

    #[token("==")]
    Eq,

    #[token("!=")]
    Neq,

    #[token("<")]
    Lss,

    #[token(">")]
    Grt,

    #[token("<=")]
    Leq,

    #[token(">=")]
    Geq,

    #[token("&&")]
    And,

    #[token("||")]
    Or,

    // Sonstige Token
    #[token(",")]
    Comma,

    #[token(";")]
    Semicolon,

    #[token("(")]
    LParen,

    #[token(")")]
    RParen,

    #[token("{")]
    LBrace,

    #[token("}")]
    RBrace,

    // Termvariablen
    #[regex(r#"[0-9]+"#)]
    ConstInt,

    #[regex(r#"(([0-9]+\.[0-9]+)|\.[0-9]+((e|E)(\+|-)?[0-9]+)?)|([0-9]+(e|E)(\+|-)?[0-9]+)"#)]
    ConstFloat,

    #[regex(r#"(true)|(false)"#)]
    ConstBoolean,

    #[regex(r#""[^\n"]*""#)]
    ConstString,

    #[regex(r#"[a-zA-Z]+[a-zA-Z0-9]*"#)]
    Id,


    #[regex(r#"[ \t\n\f]+"#, logos::skip)]
    #[regex(r#"//[^\n]*"#, logos::skip)]
    #[regex(r#"/\*([^*]|\*[^/])*\*/"#, logos::skip)]
    Skip,


    // Logos requires one token variant to handle errors,
    // it can be named anything you wish.
    #[error]
    Error,
}
